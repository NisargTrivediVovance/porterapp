package app.com.porterapp;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.viewpagerindicator.CirclePageIndicator;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import app.com.porterapp.adapter.SlidingImage_Adapter;


@EActivity(R.layout.imagesdetails)
public class ImagesDetails extends AppCompatActivity {


    @ViewById
    TextView tvHome;
    @ViewById
    TextView tvExplore;
    @ViewById
    TextView tvMap;
    @ViewById
    TextView tvLikes;
    @ViewById
    TextView tvProfile;

    @ViewById
    ViewPager mpager;

    @ViewById
    CirclePageIndicator indicator;

    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();


    @ViewById
    TextView txtdetailtile;
    String title[] = {"Free side of fries with any order!","Best BLT in NYC","$1.99 Chilly Dog"};

    @ViewById
    TextView txtMenu;
    String itemmenu[] = {"Mike's Burgers","Bo's","Phat DOG"};


    @ViewById
    TextView mealdetails;
    String []itemdetails ={"American meals, craft beers  cocktails in a" + "rustic-chic locale with a rooftop patio.","American BLT and coffee shop in the heart of NYC.","America's greatest chilly dog only $1.99. Get offer below."};

    @ViewById
    TextView km;
    String ikm[] = {"0.5 miles away","0.3  miles away","1.2  miles away"};


    @ViewById
    TextView txtoffer;
    String moffer[] = {"Get Offer","Website","Website"};

    Integer[]  IMAGES = {R.drawable.image_details_1,R.drawable.image_details_2,R.drawable.image_details_3,R.drawable.image_details_1};
    //Integer[] IMAGES_Best= {R.drawable.image_detail_best_1,R.drawable.image_detail_best_2,R.drawable.image_detail_best_3};
    Integer[] IMAGES_Best= {R.drawable.image_detail_best_1};
    Integer[] IMAGES_Chilly_dog= {R.drawable.chilly_dog_1};


    @ViewById
    ImageView imgExit;


    @AfterViews
    public void init() {
        setSelected(tvLikes, tvExplore, tvHome, tvMap, tvProfile);


        minit();
    }


    @Click
    public void imgExit(){
        finish();
    }

    public void setSelected(TextView tv1,TextView tv2,TextView tv3,TextView tv4,TextView tv5){
        tv1.setSelected(true);
        tv1.setTextColor(getResources().getColor(R.color.white));
        tv2.setSelected(false);
        tv2.setTextColor(getResources().getColor(R.color.bottom_color));
        tv3.setSelected(false);
        tv3.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv4.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv5.setTextColor(getResources().getColor(R.color.bottom_color));
    }



    private void minit() {

        try {
            txtdetailtile.setText(title[DinniglistFragment.ItemPosition]);
            txtMenu.setText(itemmenu[DinniglistFragment.ItemPosition]);
            mealdetails.setText(itemdetails[DinniglistFragment.ItemPosition]);
            km.setText(ikm[DinniglistFragment.ItemPosition]);
            txtoffer.setText(moffer[DinniglistFragment.ItemPosition]);
        } catch (Exception e) {
            e.printStackTrace();

            DinniglistFragment.ItemPosition = 0;
            txtdetailtile.setText(title[DinniglistFragment.ItemPosition]);
            txtMenu.setText(itemmenu[DinniglistFragment.ItemPosition]);
            mealdetails.setText(itemdetails[DinniglistFragment.ItemPosition]);
            km.setText(ikm[DinniglistFragment.ItemPosition]);
            txtoffer.setText(moffer[DinniglistFragment.ItemPosition]);
        }

        try {
            if(DinniglistFragment.ItemPosition==0){

                for(int i=0;i<IMAGES.length;i++)
                    ImagesArray.add(IMAGES[i]);

            }else if(DinniglistFragment.ItemPosition==1){
                for(int i=0;i<IMAGES_Best.length;i++)
                    ImagesArray.add(IMAGES_Best[i]);
            }else if(DinniglistFragment.ItemPosition == 2){

                for(int i=0;i<IMAGES_Chilly_dog.length;i++)
                    ImagesArray.add(IMAGES_Chilly_dog[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // defulat images
            for(int i=0;i<IMAGES.length;i++)
                ImagesArray.add(IMAGES[i]);
        }


//        mPager = (ViewPager) findViewById(R.id.pager);

        try {
            mpager.setAdapter(new SlidingImage_Adapter(ImagesDetails.this,ImagesArray));
            indicator.setViewPager(mpager);
        } catch (Exception e) {
            e.printStackTrace();
        }

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES =IMAGES.length;

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mpager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });



        if(ImagesArray.size()==1){
            indicator.setVisibility(View.GONE);
        }else{
            indicator.setVisibility(View.VISIBLE);
        }
    }

}