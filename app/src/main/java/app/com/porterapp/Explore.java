package app.com.porterapp;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import app.com.porterapp.utility.Utility;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.Polyline;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.FragmentByTag;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.explore)
public class Explore extends AppCompatActivity implements LocationListener,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    @ViewById
    TextView tvHome;
    @ViewById
    TextView tvExplore;
    @ViewById
    TextView tvMap;
    @ViewById
    TextView tvLikes;
    @ViewById
    TextView tvProfile;

    @ViewById
    TextView topbrookline;

    @ViewById
    ImageView arrowExplore;

    @ViewById
    ImageView imgExit;


    @ViewById
    TextView nyc;

    @ViewById
    TextView txtdining;

    @ViewById
    TextView txtshoping;

    @ViewById
    TextView topic;

    @ViewById
    LinearLayout linear1;

    @ViewById
    LinearLayout linear2;

    @ViewById
    ImageView imgstatueofliberty;

    @ViewById
     LinearLayout linear1_1;

    @ViewById
    FrameLayout frm;


    @ViewById
    ImageView imgsoho;


    @ViewById
    TextView  changeLocationleft;

    @ViewById
    TextView  topExplorer;

    @ViewById
    LinearLayout headrlinearbg;


    @ViewById
    TextView  changeLocation;

    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    private Polyline currentPolyline;
    public Location mLastLocation;
    AppPreferences appPreferences;

    boolean isDown = true;


    BroadcastReceiverFinish ActFinsh;


    public static FragmentTransaction masterFragmentManager;
//    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();


    public static FragmentManager masterFragmentManager1;

    Context mContext;


//    @FragmentById
//    ExplorerFragment myFragment;

//    @FragmentByTag

    @AfterViews
    public void init() {



        mContext = Explore.this.getApplicationContext();

        masterFragmentManager1 = getSupportFragmentManager();
//        masterFragmentManager = getSupportFragmentManager().beginTransaction();;


//
        this.ActFinsh = new BroadcastReceiverFinish();
        //return START_NOT_STICKY;
        this.registerReceiver(this.ActFinsh,  new IntentFilter("doExit"));


        appPreferences=new AppPreferences(this);
        setSelected(tvExplore, tvHome, tvMap, tvLikes, tvProfile);
        TopicSelected();
        if(getIntent()!=null) {
            if(getIntent().getStringExtra("fromadd")!=null) {
                if (getIntent().getStringExtra("fromadd").equalsIgnoreCase("yes")) {
                    startActivity(new Intent(this, ActDialog_.class));
                }
            }
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                //mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            // mMap.setMyLocationEnabled(true);
        }
        linear2.setVisibility(View.GONE);




        //Now we doing Fragment Process


        ExplorerFragment fragment = ExplorerFragment_.builder().build();

        // Begin the transaction
//        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
// Replace the contents of the container with the new fragment
        //  ft.replace(R.id.frm, new ExplorerFragment());

//        masterFragmentManager.replace(R.id.frm, fragment);

// or ft.add(R.id.your_placeholder, new FooFragment());
// Complete the changes added above
//        masterFragmentManager.commit();

//        ExplorerFragment nearFragment = new ExplorerFragment();
        Utility.SetFragment(fragment, mContext, "Explorer");


    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }
    public void setSelected(TextView tv1, TextView tv2, TextView tv3, TextView tv4, TextView tv5) {
        tv1.setSelected(true);
        tv1.setTextColor(getResources().getColor(android.R.color.white));
        tv2.setSelected(false);
        tv2.setTextColor(getResources().getColor(R.color.bottom_color));
        tv3.setSelected(false);
        tv3.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv4.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv5.setTextColor(getResources().getColor(R.color.bottom_color));
        topbrookline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManageBrooklyn();
            }
        });
        arrowExplore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManageBrooklyn();
            }
        });
    }


    @Click
    public  void txtshoping(){

        txtshoping.setBackground(getResources().getDrawable(R.drawable.boarder));
        txtshoping.setTextColor(getResources().getColor(R.color.textColor));
        txtdining.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtdining.setTextColor(getResources().getColor(R.color.black));

        topic.setBackground(getResources().getDrawable(android.R.color.transparent));
        topic.setTextColor(getResources().getColor(R.color.black));

//        finish();
//        startActivity(new Intent(this, Shoppinglist_.class))

        ShoppinglistFragment fragment = ShoppinglistFragment_.builder().build();
        Utility.SetFragment(fragment, mContext, "Shopnglist");

    }


    @Click
    public void topic(){
        TopicSelected();
    }


    public void TopicSelected(){

        topic.setBackground(getResources().getDrawable(R.drawable.boarder));
        topic.setTextColor(getResources().getColor(R.color.textColor));
        txtdining.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtdining.setTextColor(getResources().getColor(R.color.black));
        txtshoping.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtshoping.setTextColor(getResources().getColor(R.color.black));

    }

    @Click
    public void txtdining(){
        //Toast.makeText(getApplicationContext(),"Click",Toast.LENGTH_LONG).show();
        txtdining.setBackground(getResources().getDrawable(R.drawable.boarder));
        txtdining.setTextColor(getResources().getColor(R.color.textColor));

        topic.setBackground(getResources().getDrawable(android.R.color.transparent));
        topic.setTextColor(getResources().getColor(R.color.black));

        txtshoping.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtshoping.setTextColor(getResources().getColor(R.color.black));

//        finish();
        //startActivity(new Intent(this, Dininglist_.class));
    }
    @Click
    public  void linear2(){
        //finish();
//        startActivity(new Intent(this, Imagesstatueofliberty_.class));
    }


    @Click
    public  void imgsoho(){
        //finish();
        startActivity(new Intent(this, Imagesstatueofliberty_.class));
    }


    @Click
    public  void imgstatueofliberty(){
        //finish();
        startActivity(new Intent(this, Imagesstatueofliberty_.class));
    }

    @Click
    public void tvHome() {
        finish();
        startActivity(new Intent(this, Home_.class));
    }

    @Click
    public void tvExplore() {
        finish();
        startActivity(new Intent(this, Explore_.class));
    }

    @Click
    public void tvMap() {
        finish();
        startActivity(new Intent(this, Map_.class));
    }

    @Click
    public void tvLikes() {
        finish();
        startActivity(new Intent(this, Likes_.class));
    }

    @Click
    public void tvProfile() {
        finish();
        startActivity(new Intent(this, Profile_.class));
    }

    /*@Override
    public void onBackPressed() {
        finish();
    }*/


    @Click
    public  void imgExit(){
        doExit();
    }
    @Override
    public void onBackPressed() {
        doExit();
    }


    public  void doExit(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("PorterApp")
                .setMessage("Are you sure you want to close this activity?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        Intent intent = new Intent();
                        intent.setAction("doExit");
                        intent.putExtra("data","Notice me senpai!");
                        sendBroadcast(intent);

                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }



    public void ManageBrooklyn() {

        if (isDown) {
            isDown = false;
            arrowExplore.setRotation(270f);
            imgExit.setVisibility(View.VISIBLE);

            linear1.setVisibility(View.GONE);
            linear1_1.setVisibility(View.VISIBLE);
            linear2.setVisibility(View.VISIBLE);
//            frm.setVisibility(View.GONE);

            headrlinearbg.setBackgroundColor(getResources().getColor(R.color.bgtransperant));
            nyc.setVisibility(View.GONE);

            topExplorer.setVisibility(View.GONE);
            changeLocation.setVisibility(View.GONE);
            changeLocationleft.setVisibility(View.VISIBLE);

        } else {

            isDown = true;
            arrowExplore.setRotation(180f);
            imgExit.setVisibility(View.GONE);
            nyc.setVisibility(View.VISIBLE);
            linear2.setVisibility(View.GONE);
            linear1_1.setVisibility(View.GONE);
            linear1.setVisibility(View.VISIBLE);
            frm.setVisibility(View.VISIBLE);

            topExplorer.setVisibility(View.VISIBLE);
            changeLocation.setVisibility(View.VISIBLE);
            changeLocationleft.setVisibility(View.GONE);

            headrlinearbg.setBackgroundColor(getResources().getColor(android.R.color.transparent));

        }

    }

    @Override
    public void onLocationChanged(Location location) {

        appPreferences.set("lat",location.getLatitude()+"");
        appPreferences.set("lng",location.getLongitude()+"");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new com.google.android.gms.location.LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    appPreferences.set("lat",location.getLatitude()+"");
                    appPreferences.set("lng",location.getLongitude()+"");
//                    CameraPosition cameraPosition = new CameraPosition.Builder()
//                            .target(new LatLng(location.getLatitude(),location.getLongitude()))
//                            .zoom(14F)
//                            .build();
//                    mMap.setMaxZoomPreference(100f);
//                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                    mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(),location.getLongitude())).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).flat(true));
                }
            });
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    protected void onResume() {
        super.onResume();

        TopicSelected();
    }




    public class BroadcastReceiverFinish extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            //contentTxt.setText(String.valueOf(level) + "%");
            finish();
        }
    };



}
