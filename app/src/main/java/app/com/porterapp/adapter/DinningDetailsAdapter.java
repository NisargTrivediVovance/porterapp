package app.com.porterapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import app.com.porterapp.R;
import app.com.porterapp.minterface.ItemRecycleClick;
import app.com.porterapp.pojo.DiningData;

public class DinningDetailsAdapter extends RecyclerView.Adapter<DinningDetailsAdapter.MyViewHolder> {

    private List<DiningData> ImageList;

//    ItemRecycleClick


    ItemRecycleClick objitem;

    Context mContext;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;


        public MyViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.imageView);
        }
    }


    public DinningDetailsAdapter(Context context,List<DiningData> moviesList, ItemRecycleClick   mitemclick) {
        this.ImageList = moviesList;
        this.objitem = mitemclick;
        this.mContext = context;
    }

    @Override
    public DinningDetailsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_dining_details, parent, false);

        return new DinningDetailsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DinningDetailsAdapter.MyViewHolder holder, final int position) {

        DiningData data = ImageList.get(position);
        try {
            Glide.with(mContext).load(mContext.getDrawable(data.getName())).override(1080, 600).into(holder.imageView);

//            holder.imageView.setImageResource(data.getName());
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    objitem.RecycleClick(position);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return ImageList.size();
    }



    public void SetRecyl(ItemRecycleClick myclick){
        this.objitem = myclick;
    }
}
