package app.com.porterapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import app.com.porterapp.adapter.DinningAdapter;
import app.com.porterapp.adapter.DinningDetailsAdapter;
import app.com.porterapp.adapter.ShopingDetailsAdapte;
import app.com.porterapp.minterface.ItemRecycleClick;
import app.com.porterapp.pojo.DiningData;
import app.com.porterapp.utility.Utility;


@EFragment(R.layout.dinninglist)
public class DinniglistFragment extends Fragment {


    @ViewById
    TextView tvHome;
    @ViewById
    TextView tvExplore;
    @ViewById
    TextView tvMap;
    @ViewById
    TextView tvLikes;
    @ViewById
    TextView tvProfile;
    @ViewById
    TextView topbrookline;
    @ViewById
    ImageView arrowExplore;

    @ViewById
    ImageView imgExit;


    @ViewById
    TextView nyc;

    @ViewById
    TextView txtdining;

    @ViewById
    TextView txtshoping;

    @ViewById
    TextView topic;

    @ViewById
    LinearLayout linear1;

    @ViewById
    LinearLayout linear2;

    @ViewById
    ImageView imgstatueofliberty;

    @ViewById
    LinearLayout linear1_1;


    @ViewById
    FrameLayout rfrm;


    @ViewById
    ImageView imgsoho;


    @ViewById
    TextView  changeLocationleft;

    @ViewById
    TextView  topExplorer;

    @ViewById
    LinearLayout headrlinearbg;


    @ViewById
    TextView  changeLocation;


    Context mContext;

    boolean isDown = true;

    DinningAdapter mAdapter;

    List<DiningData> mDl;


    @ViewById
    RecyclerView mRecyclerView;



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;

    }


    @AfterViews
    void calledAfterviewInjection(){

        setSelected(tvExplore, tvHome, tvMap, tvLikes, tvProfile);
//        TopicSelected();
//        DiningSelected();

        Dining_List();
        DiningSelected();
    }

    @Click
    public  void txtshoping(){

        txtshoping.setBackground(getResources().getDrawable(R.drawable.boarder));
        txtshoping.setTextColor(getResources().getColor(R.color.textColor));
        txtdining.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtdining.setTextColor(getResources().getColor(R.color.black));
        topic.setBackground(getResources().getDrawable(android.R.color.transparent));
        topic.setTextColor(getResources().getColor(R.color.black));

//        finish();
//        startActivity(new Intent(mContext, Shoppinglist_.class));




        ShoppinglistFragment fragment = ShoppinglistFragment_.builder().build();
        Utility.SetFragment(fragment, mContext, "Shopnglist");

    }

    @Click
    public void topic(){
        TopicSelected();
    }


    public void TopicSelected(){

        topic.setBackground(getResources().getDrawable(R.drawable.boarder));
        topic.setTextColor(getResources().getColor(R.color.textColor));
        txtdining.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtdining.setTextColor(getResources().getColor(R.color.black));
        txtshoping.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtshoping.setTextColor(getResources().getColor(R.color.black));

        ExplorerFragment fragment = ExplorerFragment_.builder().build();
        Utility.SetFragment(fragment, mContext, "Explorer");

    }


    @Click
    public  void linear2(){
        //finish();
        startActivity(new Intent(mContext, Imagesstatueofliberty_.class));
    }


    @Click
    public  void imgsoho(){
        //finish();
        startActivity(new Intent(mContext, Imagesstatueofliberty_.class));
    }


    @Click
    public  void imgstatueofliberty(){
        //finish();
        startActivity(new Intent(mContext, Imagesstatueofliberty_.class));
    }

    @Click
    public void tvHome() {
        startActivity(new Intent(mContext, Home_.class));
    }

    @Click
    public void tvExplore() {
//        startActivity(new Intent(mContext, Explore_.class));
    }

    @Click
    public void tvMap() {
        startActivity(new Intent(mContext, Map_.class));
    }

    @Click
    public void tvLikes() {
        startActivity(new Intent(mContext, Likes_.class));
    }

    @Click
    public void tvProfile() {
        startActivity(new Intent(mContext, Profile_.class));
    }

    /*@Override
    public void onBackPressed() {
        finish();
    }*/


  /*  public void setSelected(TextView tv1, TextView tv2, TextView tv3, TextView tv4, TextView tv5) {
        tv1.setSelected(true);
        tv1.setTextColor(getResources().getColor(android.R.color.white));
        tv2.setSelected(false);
        tv2.setTextColor(getResources().getColor(R.color.bottom_color));
        tv3.setSelected(false);
        tv3.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv4.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv5.setTextColor(getResources().getColor(R.color.bottom_color));
        topbrookline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        arrowExplore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }*/



    public void setSelected(TextView tv1,TextView tv2,TextView tv3,TextView tv4,TextView tv5){

        tv1.setSelected(true);
        tv1.setTextColor(getResources().getColor(android.R.color.white));
        tv2.setSelected(false);
        tv2.setTextColor(getResources().getColor(R.color.bottom_color));
        tv3.setSelected(false);
        tv3.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv4.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv5.setTextColor(getResources().getColor(R.color.bottom_color));
    }



    @Click
    public void txtdining(){
        //Toast.makeText(getApplicationContext(),"Click",Toast.LENGTH_LONG).show();
        DiningSelected();
        Dining_List();
    }


    private void DiningSelected(){

        txtdining.setBackground(getResources().getDrawable(R.drawable.boarder));
        txtdining.setTextColor(getResources().getColor(R.color.textColor));

        topic.setBackground(getResources().getDrawable(android.R.color.transparent));
        topic.setTextColor(getResources().getColor(R.color.black));

        txtshoping.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtshoping.setTextColor(getResources().getColor(R.color.black));

//        startActivity(new Intent(this, Dininglist_.class));

    }


    private void prepareData(){

        mDl = new ArrayList<DiningData>();

        DiningData obj = new DiningData();
        obj.setName(R.drawable.dinning1);
//        obj.setName(R.drawable.dinning_image_1);

        mDl.add(obj);


        DiningData obj2 = new DiningData();
        obj2.setName(R.drawable.dinning2);
        mDl.add(obj2);

        DiningData obj3 = new DiningData();
        obj3.setName(R.drawable.dinning3);
        mDl.add(obj3);


        DiningData obj4 = new DiningData();
        obj4.setName(R.drawable.dinning4);
        mDl.add(obj4);

        DiningData obj5 = new DiningData();
        obj5.setName(R.drawable.dinning5);
        mDl.add(obj5);
    }
    //-------------------------------------------------------1. DiningList Data-------------------------------

    public  void Dining_List(){

        prepareData();

        try {
            mAdapter = new DinningAdapter(mContext,mDl, new ItemRecycleClick() {
                @Override
                public void RecycleClick(int pos) {

                    // startActivity(new Intent(Dininglist.this, DinningDetails_.class));

                    if(pos<1) {
                        Details_Scren_process_here();
                    }



                }
            });

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    //--------------------------------------------------------------Details Screen Data-------------------------------------
    public  static int ItemPosition = 0;

    DinningDetailsAdapter m_dining_details_Adapter;

    public void Details_Scren_process_here(){

        PrepareData_Details();

        try {
            m_dining_details_Adapter = new DinningDetailsAdapter(mContext,mDl, new ItemRecycleClick() {
                @Override
                public void RecycleClick(int pos) {

                    ItemPosition = pos;
                    startActivity(new Intent(mContext, ImagesDetails_.class));


                }
            });

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(m_dining_details_Adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        DiningSelected();

    }


    public  void PrepareData_Details(){

        mDl = new ArrayList<DiningData>();

        DiningData obj = new DiningData();
        obj.setName(R.drawable.dinning_details_1);
        mDl.add(obj);


        DiningData obj2 = new DiningData();
        obj2.setName(R.drawable.dinning_details_2);
        mDl.add(obj2);

        DiningData obj3 = new DiningData();
        obj3.setName(R.drawable.dinning_details_3);
        mDl.add(obj3);


        DiningData obj4 = new DiningData();
        obj4.setName(R.drawable.dinning_details_4);
        mDl.add(obj4);


        DiningData obj5 = new DiningData();
        obj5.setName(R.drawable.shopping_detail_4);
        mDl.add(obj5);


        DiningData obj6 = new DiningData();
        obj6.setName(R.drawable.shopping_detail_4);
        mDl.add(obj6);


        DiningData obj7 = new DiningData();
        obj7.setName(R.drawable.shopping_detail_4);
        mDl.add(obj7);
    }

    //-------------------------------------------------------2. ShopingList Data-------------------------------

    private void ShppingSelected(){

        txtshoping.setBackground(getResources().getDrawable(R.drawable.boarder));
        txtshoping.setTextColor(getResources().getColor(R.color.textColor));

        txtdining.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtdining.setTextColor(getResources().getColor(R.color.black));

        topic.setBackground(getResources().getDrawable(android.R.color.transparent));
        topic.setTextColor(getResources().getColor(R.color.black));
    }


    public  void Shoping_List(){

        prepareData_shopinglist();

        try {
            mAdapter = new DinningAdapter(mContext,mDl, new ItemRecycleClick() {
                @Override
                public void RecycleClick(int pos) {
                    // startActivity(new Intent(Shoppinglist.this, ShoppinglistDetails_.class));

                    if(pos==0) {
                        Details_Scren_process_shoping_here();
                    }

                }
            });

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    private void prepareData_shopinglist(){

        mDl = new ArrayList<DiningData>();

        DiningData obj = new DiningData();
        obj.setName(R.drawable.shopping_1);
        mDl.add(obj);


        DiningData obj2 = new DiningData();
        obj2.setName(R.drawable.shopping_2);
        mDl.add(obj2);

        DiningData obj3 = new DiningData();
        obj3.setName(R.drawable.shopping_3);
        mDl.add(obj3);


        DiningData obj4 = new DiningData();
        obj4.setName(R.drawable.shopping_4);
        mDl.add(obj4);

        DiningData obj5 = new DiningData();
        obj5.setName(R.drawable.shopping_5);
        mDl.add(obj5);

        DiningData obj6 = new DiningData();
        obj6.setName(R.drawable.shopping_6);
        mDl.add(obj6);

        DiningData obj7 = new DiningData();
        obj7.setName(R.drawable.shopping_7);
        mDl.add(obj7);

    }


    //-------------------------------------------------------------- Details Screen Data-------------------------------------

    public void Details_Scren_process_shoping_here(){

        ShopingDetailsAdapte mAdapter;

        PrepareData_Shoping_Details();

        try {
            mAdapter = new ShopingDetailsAdapte(mContext,mDl, new ItemRecycleClick() {
                @Override
                public void RecycleClick(int pos) {

                    if(pos<=1) {
                        ItemPosition = pos;
                        startActivity(new Intent(mContext, ImagesDetailsShopping_.class));
                    }

                }
            });

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public  void PrepareData_Shoping_Details(){

        mDl = new ArrayList<DiningData>();

        DiningData obj = new DiningData();
        obj.setName(R.drawable.shopping_detail_1);
        mDl.add(obj);

        DiningData obj2 = new DiningData();
        obj2.setName(R.drawable.shopping_detail_2);
        mDl.add(obj2);

        DiningData obj3 = new DiningData();
        obj3.setName(R.drawable.shopping_detail_3);
        mDl.add(obj3);

        DiningData obj4 = new DiningData();
        obj4.setName(R.drawable.shopping_detail_4);
        mDl.add(obj4);

        DiningData obj5 = new DiningData();
        obj5.setName(R.drawable.shopping_detail_4);
        mDl.add(obj5);

        DiningData obj6 = new DiningData();
        obj6.setName(R.drawable.shopping_detail_4);
        mDl.add(obj6);

        DiningData obj7 = new DiningData();
        obj7.setName(R.drawable.shopping_detail_4);
        mDl.add(obj7);

    }



    public class BroadcastReceiverFinish extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            //contentTxt.setText(String.valueOf(level) + "%");
           // mContext.finish();
        }
    };

}

