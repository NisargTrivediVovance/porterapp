package app.com.porterapp;


import android.content.Context;
import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import app.com.porterapp.adapter.DinningAdapter;
import app.com.porterapp.adapter.ShopingDetailsAdapte;
import app.com.porterapp.minterface.ItemRecycleClick;
import app.com.porterapp.pojo.DiningData;
import app.com.porterapp.utility.Utility;

@EFragment(R.layout.dinninglist)
public class ShoppinglistFragment  extends Fragment {

    DinningAdapter mAdapter;

    @ViewById
    TextView tvHome;
    @ViewById
    TextView tvExplore;
    @ViewById
    TextView tvMap;
    @ViewById
    TextView tvLikes;
    @ViewById
    TextView tvProfile;
    @ViewById
    RecyclerView mRecyclerView;
    List<DiningData> mDl;

    @ViewById
    TextView txtdining;

    @ViewById
    TextView txtshoping;

    @ViewById
    TextView topic;



    Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;

    }


    @AfterViews
    public void init()
    {
        setSelected(tvExplore,tvLikes,tvHome,tvMap,tvProfile);


//        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);





       /* mRecyclerView.setAdapter(new DinningAdapter(mDl, new DinningAdapter.OnItemClickListener() {
            @Override public void onItemClick(ContentItem item) {
                Toast.makeText(getContext(), "Item Clicked", Toast.LENGTH_LONG).show();
            }
        }));*/

        Shoping_List();
        ShppingSelected();
    }



    private void prepareData_shopinglist(){

        mDl = new ArrayList<DiningData>();

        DiningData obj = new DiningData();
        obj.setName(R.drawable.shopping_1);
        mDl.add(obj);


        DiningData obj2 = new DiningData();
        obj2.setName(R.drawable.shopping_2);
        mDl.add(obj2);

        DiningData obj3 = new DiningData();
        obj3.setName(R.drawable.shopping_3);
        mDl.add(obj3);


        DiningData obj4 = new DiningData();
        obj4.setName(R.drawable.shopping_4);
        mDl.add(obj4);

        DiningData obj5 = new DiningData();
        obj5.setName(R.drawable.shopping_5);
        mDl.add(obj5);


        DiningData obj6 = new DiningData();
        obj6.setName(R.drawable.shopping_6);
        mDl.add(obj6);


        DiningData obj7 = new DiningData();
        obj7.setName(R.drawable.shopping_7);
        mDl.add(obj7);

    }




    public void setSelected(TextView tv1,TextView tv2,TextView tv3,TextView tv4,TextView tv5){
        /*tv1.setSelected(false);
        tv1.setTextColor(getResources().getColor(R.color.bottom_color));
        tv2.setSelected(true);
        tv2.setTextColor(getResources().getColor(android.R.color.transparent));

        tv3.setSelected(false);
        tv3.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv4.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv5.setTextColor(getResources().getColor(R.color.bottom_color));*/


        tv1.setSelected(true);
        tv1.setTextColor(getResources().getColor(android.R.color.white));
        tv2.setSelected(false);
        tv2.setTextColor(getResources().getColor(R.color.bottom_color));
        tv3.setSelected(false);
        tv3.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv4.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv5.setTextColor(getResources().getColor(R.color.bottom_color));
    }
    @Click
    public void tvHome(){
//        finish();
        startActivity(new Intent(mContext, Home_.class));
    }
    @Click
    public void tvExplore(){
//        finish();
        startActivity(new Intent(mContext, Explore_.class));
    }
    @Click
    public void tvMap(){
//        finish();
        startActivity(new Intent(mContext, Map_.class));
    }
    @Click
    public void tvLikes(){
//        finish();
        startActivity(new Intent(mContext, Likes_.class));
    }
    @Click
    public void tvProfile(){
//        finish();
        startActivity(new Intent(mContext, Profile_.class));
    }

  /*  @Override
    public void onBackPressed() {
//        finish();
    }*/






    @Click
    public  void txtshoping(){

        ShppingSelected();
        Shoping_List();
    }


    @Click
    public void topic(){
        topic.setBackground(getResources().getDrawable(R.drawable.boarder));
        topic.setTextColor(getResources().getColor(R.color.textColor));
        txtdining.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtdining.setTextColor(getResources().getColor(R.color.black));
        txtshoping.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtshoping.setTextColor(getResources().getColor(R.color.black));
//        finish();
        startActivity(new Intent(mContext, Explore_.class));
    }

    @Click
    public void txtdining(){
        //Toast.makeText(getApplicationContext(),"Click",Toast.LENGTH_LONG).show();
        DiningSelected();
    }



    private void DiningSelected(){

        txtdining.setBackground(getResources().getDrawable(R.drawable.boarder));
        txtdining.setTextColor(getResources().getColor(R.color.textColor));

        topic.setBackground(getResources().getDrawable(android.R.color.transparent));
        topic.setTextColor(getResources().getColor(R.color.black));

        txtshoping.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtshoping.setTextColor(getResources().getColor(R.color.black));

//        finish();
//        startActivity(new Intent(mContext, Dininglist_.class));


        DinniglistFragment fragment = DinniglistFragment_.builder().build();
        Utility.SetFragment(fragment, mContext, "Dinning");

    }



    private void ShppingSelected(){

        txtshoping.setBackground(getResources().getDrawable(R.drawable.boarder));
        txtshoping.setTextColor(getResources().getColor(R.color.textColor));

        txtdining.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtdining.setTextColor(getResources().getColor(R.color.black));

        topic.setBackground(getResources().getDrawable(android.R.color.transparent));
        topic.setTextColor(getResources().getColor(R.color.black));


    }



    //-------------------------------------------------------1. DiningList Data-------------------------------

    public  void Shoping_List(){

        prepareData_shopinglist();

        try {
            mAdapter = new DinningAdapter(mContext,mDl, new ItemRecycleClick() {
                @Override
                public void RecycleClick(int pos) {
                    // startActivity(new Intent(Shoppinglist.this, ShoppinglistDetails_.class));

                    if(pos==0) {
                        Details_Scren_process_shoping_here();
                    }

                }
            });

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //=================================================


    //--------------------------------------------------------------Details Screen Data-------------------------------------
//    public  static int ItemPosition = 0;

    public void Details_Scren_process_shoping_here(){

        ShopingDetailsAdapte mAdapter;

        PrepareData_Shoping_Details();

        try {
            mAdapter = new ShopingDetailsAdapte(mContext,mDl, new ItemRecycleClick() {
                @Override
                public void RecycleClick(int pos) {

                    if(pos<=1) {
                        Dininglist.ItemPosition = pos;

//                        Toast.makeText(mContext,""+pos,Toast.LENGTH_LONG).show();
                        startActivity(new Intent(mContext, ImagesDetailsShopping_.class));
                    }

                }
            });

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public  void PrepareData_Shoping_Details(){

        mDl = new ArrayList<DiningData>();

        DiningData obj = new DiningData();
        obj.setName(R.drawable.shopping_detail_1);
        mDl.add(obj);


        DiningData obj2 = new DiningData();
        obj2.setName(R.drawable.shopping_detail_2);
        mDl.add(obj2);

        DiningData obj3 = new DiningData();
        obj3.setName(R.drawable.shopping_detail_3);
        mDl.add(obj3);


        DiningData obj4 = new DiningData();
        obj4.setName(R.drawable.shopping_detail_4);
        mDl.add(obj4);

        DiningData obj5 = new DiningData();
        obj5.setName(R.drawable.shopping_detail_4);
        mDl.add(obj5);


        DiningData obj6 = new DiningData();
        obj6.setName(R.drawable.shopping_detail_4);
        mDl.add(obj6);


        DiningData obj7 = new DiningData();
        obj7.setName(R.drawable.shopping_detail_4);
        mDl.add(obj7);

    }


}
