package app.com.porterapp;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.jama.carouselview.CarouselScrollListener;
import com.jama.carouselview.CarouselViewListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EActivity(R.layout.map)
public class Map extends AppCompatActivity  implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,TaskLoadedCallback {


    private GoogleMap mMap;
    @ViewById
    TextView tvHome;
    @ViewById
    TextView tvExplore;
    @ViewById
    TextView tvMap;
    @ViewById
    TextView tvLikes;
    @ViewById
    TextView tvProfile;
    @ViewById
    TextView n;
    @ViewById
    LinearLayout ll;
    @ViewById
    TextView brooklyn;
    @ViewById
    TextView greenpoint;
    @ViewById
    TextView midtown;
    @ViewById
    TextView soho;
    @ViewById
    EditText edtSearch;
    @ViewById
    ImageView search;
    @ViewById
    com.jama.carouselview.CarouselView cv;



    private Animation slideLeft;

    private int[] images = {R.drawable.h1,
            R.drawable.h2, R.drawable.h3};

    ArrayList<HotelModel> list=new ArrayList<>();
    ArrayList<HotelModel> filterlist=new ArrayList<>();
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    private Polyline currentPolyline;
    public Location mLastLocation;

    AppPreferences appPreferences;
    Animator animationUtils;


    @AfterViews
    public void init()
    {
        appPreferences=new AppPreferences(this);
        setSelected(tvMap,tvExplore,tvHome,tvLikes,tvProfile);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        slideLeft = AnimationUtils.loadAnimation(this,R.anim.slide_in_left);

        edtSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (edtSearch.getRight() - edtSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                        edtSearch.startAnimation(animationUtils);
                        //circleReveal(R.id.edtSearch,1,true,false);

                        search.setVisibility(View.VISIBLE);

                        cv.setVisibility(View.VISIBLE);
                        KeyBoardHandling.hideSoftKeyboard(Map.this);
                        edtSearch.setVisibility(View.GONE);


                        return true;
                    }
                }
                return false;
            }
        });

        bindData();
        if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) Map.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(Map.this);
                alertBuilder.setCancelable(true);
                alertBuilder.setTitle("Permission necessary");
                alertBuilder.setMessage("Write calendar permission is necessary to write event!!!");
                alertBuilder.setPositiveButton(android.R.string.yes, (dialog, which) -> ActivityCompat.requestPermissions((Activity)Map.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1000));
                AlertDialog alert = alertBuilder.create();
                alert.show();

            } else {
                ActivityCompat.requestPermissions((Activity)Map.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1000);
            }
        } else {
//            permission();
        }

    }

    public void bind(){
        cv.setSize(filterlist.size());
        cv.setResource(R.layout.start_carousel_movies_item);
        cv.setScaleOnScroll(true);
        cv.setSpacing(10);
        cv.hideIndicator(true);
        cv.setCarouselScrollListener(new CarouselScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState, int position) {
                System.out.println("CALLED====>"+newState);
                if(recyclerView.getScrollState()==RecyclerView.SCROLL_STATE_IDLE) {
                    if (mLastLocation != null) {
                        mMap.clear();
                        LatLng l1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                        LatLng l2 = new LatLng(Double.parseDouble(filterlist.get(position).lat), Double.parseDouble(filterlist.get(position).lng));

                        mMap.addMarker(new MarkerOptions().position(l1).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));
                        mMap.addMarker(new MarkerOptions().position(l2).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));

                        new FetchURL(Map.this).execute(getUrl(l1, l2, "driving"), "driving");

//                    CameraPosition cameraPosition = new CameraPosition.Builder()
//                            .target(new LatLng(l1.latitude, l1.longitude))
//                            .zoom(12.5F)
//                            .build();
//                    mMap.setMaxZoomPreference(100f);
//                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                }

            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

            }
        });
        cv.setCarouselViewListener(new CarouselViewListener() {
            @Override
            public void onBindView(View view, final int position) {

                ImageView imageView = view.findViewById(R.id.imageView);
                imageView.setImageDrawable(getDrawable(filterlist.get(position).HotelImage));

                TextView textView = view.findViewById(R.id.textViewTitle);
                textView.setText(filterlist.get(position).HotelName);

                TextView tv2 = view.findViewById(R.id.tv2);
                if(TextUtils.isEmpty(appPreferences.getString("lat")))
                    tv2.setText("0 miles");
                else
                    tv2.setText(String.format("%.2f", (distance(Double.parseDouble(appPreferences.getString("lat")), Double.parseDouble(appPreferences.getString("lng")), Double.parseDouble(filterlist.get(position).lat), Double.parseDouble(filterlist.get(position).lng)))*0.62137) + " miles");




            }
        });
        cv.show();

    }
    private String getUrl(LatLng origin, LatLng dest, String directionMode) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Mode
        String mode = "mode=" + directionMode;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + "AIzaSyAIcOA4DO5g7XpP-QBqhbkN1vZZM5jyMxU";
        return url;
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @Click
    public void search(){
//        edtSearch.setVisibility(View.VISIBLE);

        edtSearch.setVisibility(View.VISIBLE);
        search.setVisibility(View.GONE);
        cv.setVisibility(View.INVISIBLE);
    }

    public void setSelected(TextView tv1,TextView tv2,TextView tv3,TextView tv4,TextView tv5){
        tv1.setSelected(true);
        tv1.setTextColor(getResources().getColor(R.color.white));
        tv2.setSelected(false);
        tv2.setTextColor(getResources().getColor(R.color.bottom_color));
        tv3.setSelected(false);
        tv3.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv4.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv5.setTextColor(getResources().getColor(R.color.bottom_color));
    }
    @Click
    public void tvHome(){
        finish();
        startActivity(new Intent(this,Home_.class));
    }
    @Click
    public void tvExplore(){
        finish();
        startActivity(new Intent(this,Explore_.class));
    }
    @Click
    public void tvMap(){
        finish();
        startActivity(new Intent(this, Map_.class));
    }
    @Click
    public void tvLikes(){
        finish();
        startActivity(new Intent(this,Likes_.class));
    }
    @Click
    public void tvProfile(){
        finish();
        startActivity(new Intent(this,Profile_.class));
    }

    @Click
    public void send(){
        mMap.clear();
        if(mLastLocation!=null) {
            LatLng l1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            LatLng l2 = new LatLng(Double.parseDouble(list.get(0).lat), Double.parseDouble(list.get(0).lng));

            mMap.addMarker(new MarkerOptions().position(l1).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));
            mMap.addMarker(new MarkerOptions().position(l2).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));

            new FetchURL(Map.this).execute(getUrl(l1, l2, "driving"), "driving");
            mMap.moveCamera(CameraUpdateFactory.newLatLng(l1));
        }


    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        if(!TextUtils.isEmpty(appPreferences.getString("lat"))) {
            LatLng sydney = new LatLng(Double.parseDouble(appPreferences.getString("lat")), Double.parseDouble(appPreferences.getString("lng")));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));


            LatLng l2 = new LatLng(Double.parseDouble(list.get(0).lat), Double.parseDouble(list.get(0).lng));

            mMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));
            mMap.addMarker(new MarkerOptions().position(l2).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));

            new FetchURL(Map.this).execute(getUrl(sydney, l2, "driving"), "driving");
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(appPreferences.getString("lat")), Double.parseDouble(appPreferences.getString("lng"))))
                    .zoom(14F)
                    .build();
            mMap.setMaxZoomPreference(100f);
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                //mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            // mMap.setMyLocationEnabled(true);
        }
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                cv.setSize(list.size());
                cv.setResource(R.layout.start_carousel_movies_item);
                cv.setScaleOnScroll(true);
                cv.setSpacing(10);
                cv.hideIndicator(true);
                cv.setCarouselScrollListener(new CarouselScrollListener() {
                    @Override
                    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState, int position) {
                        mMap.clear();

                        if(recyclerView.getScrollState()==RecyclerView.SCROLL_STATE_IDLE){

                            if(mLastLocation!=null) {
                                LatLng l1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                                LatLng l2 = new LatLng(Double.parseDouble(list.get(position).lat), Double.parseDouble(list.get(position).lng));

                                mMap.addMarker(new MarkerOptions().position(l1).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));
                                mMap.addMarker(new MarkerOptions().position(l2).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));

                                new FetchURL(Map.this).execute(getUrl(l1, l2, "driving"), "driving");

                                    CameraPosition cameraPosition = new CameraPosition.Builder()
                                            .target(new LatLng(l1.latitude, l1.longitude))
                                            .zoom(12.5F)
                                            .build();
                                    mMap.setMaxZoomPreference(100f);
                                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            }
                        }
//

                    }

                    @Override
                    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                        System.out.println("CALLED======================================>dx and dy"+recyclerView.getScrollState());
                    }
                });
                cv.setCarouselViewListener(new CarouselViewListener() {
                    @Override
                    public void onBindView(View view, final int position) {
                        ImageView imageView = view.findViewById(R.id.imageView);
                        imageView.setImageDrawable(getDrawable(list.get(position).HotelImage));

                        TextView textView = view.findViewById(R.id.textViewTitle);
                        textView.setText(list.get(position).HotelName);

                        TextView tv2 = view.findViewById(R.id.tv2);
                        if(TextUtils.isEmpty(appPreferences.getString("lat")))
                            tv2.setText("0 miles");
                        else
                        tv2.setText(String.format("%.2f", (distance(Double.parseDouble(appPreferences.getString("lat")), Double.parseDouble(appPreferences.getString("lng")), Double.parseDouble(list.get(position).lat), Double.parseDouble(list.get(position).lng)))*0.62137) + " miles");


                    }
                });
                cv.show();
            }
        });
    }
    public void setFirst(){
        cv.setSize(list.size());
        cv.setResource(R.layout.start_carousel_movies_item);
        cv.setScaleOnScroll(true);
        cv.setSpacing(10);
        cv.hideIndicator(true);
        cv.setCarouselScrollListener(new CarouselScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState, int position) {
                mMap.clear();

                if(recyclerView.getScrollState()==RecyclerView.SCROLL_STATE_IDLE){

                    if(mLastLocation!=null) {
                        LatLng l1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                        LatLng l2 = new LatLng(Double.parseDouble(list.get(position).lat), Double.parseDouble(list.get(position).lng));

                        mMap.addMarker(new MarkerOptions().position(l1).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));
                        mMap.addMarker(new MarkerOptions().position(l2).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));

                        new FetchURL(Map.this).execute(getUrl(l1, l2, "driving"), "driving");

                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(l1.latitude, l1.longitude))
                            .zoom(12.5F)
                            .build();
                    mMap.setMaxZoomPreference(100f);
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                }
//

            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                System.out.println("CALLED======================================>dx and dy"+recyclerView.getScrollState());
            }
        });
        cv.setCarouselViewListener(new CarouselViewListener() {
            @Override
            public void onBindView(View view, final int position) {
                ImageView imageView = view.findViewById(R.id.imageView);
                imageView.setImageDrawable(getDrawable(list.get(position).HotelImage));

                TextView textView = view.findViewById(R.id.textViewTitle);
                textView.setText(list.get(position).HotelName);

                TextView tv2 = view.findViewById(R.id.tv2);
                if(TextUtils.isEmpty(appPreferences.getString("lat")))
                    tv2.setText("0 miles");
                else
                    tv2.setText(String.format("%.2f", (distance(Double.parseDouble(appPreferences.getString("lat")), Double.parseDouble(appPreferences.getString("lng")), Double.parseDouble(list.get(position).lat), Double.parseDouble(list.get(position).lng)))*0.62137) + " miles");
            }
        });
        cv.show();
    }
    @Click
    public void ll(){
        if(brooklyn.getVisibility()==View.VISIBLE){
           normalView();
           n.setText("Neighborhood");
                setFirst();
                LatLng l1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                LatLng l2 = new LatLng(Double.parseDouble(list.get(0).lat), Double.parseDouble(list.get(0).lng));

                mMap.addMarker(new MarkerOptions().position(l1).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));
                mMap.addMarker(new MarkerOptions().position(l2).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));

                new FetchURL(Map.this).execute(getUrl(l1, l2, "driving"), "driving");

        }else {
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) n.getLayoutParams();
            params.width = getResources().getDimensionPixelSize(R.dimen._150sdp);
            n.setLayoutParams(params);
            n.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.drawable_close, 0);
            brooklyn.setVisibility(View.VISIBLE);
            greenpoint.setVisibility(View.VISIBLE);
            midtown.setVisibility(View.VISIBLE);
            soho.setVisibility(View.VISIBLE);

        }
    }
    private void normalView(){
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) n.getLayoutParams();
        params.width = getResources().getDimensionPixelSize(R.dimen._100sdp);
        n.setLayoutParams(params);
        n.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
        brooklyn.setVisibility(View.GONE);
        greenpoint.setVisibility(View.GONE);
        midtown.setVisibility(View.GONE);
        soho.setVisibility(View.GONE);
    }
    @Click
    public void greenpoint(){
        n.setText("Greenpoint");
        normalView();
        filterlist.clear();
        if(n.getText().toString().equalsIgnoreCase("Greenpoint")){
            for(int i=0;i<list.size();i++) {
                if (mLastLocation != null) {
                    if(n.getText().toString().equalsIgnoreCase(list.get(i).City)) {
                        filterlist.add(list.get(i));
                    }
                }
            }
            bind();
            LatLng l1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            LatLng l2 = new LatLng(Double.parseDouble(filterlist.get(0).lat), Double.parseDouble(filterlist.get(0).lng));

            mMap.addMarker(new MarkerOptions().position(l1).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));
            mMap.addMarker(new MarkerOptions().position(l2).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));

            new FetchURL(Map.this).execute(getUrl(l1, l2, "driving"), "driving");
        }
    }
    @Click
    public void midtown(){
        n.setText("Midtown");
        normalView();
        filterlist.clear();
        if(n.getText().toString().equalsIgnoreCase("Midtown")){
            for(int i=0;i<list.size();i++) {
                if (mLastLocation != null) {
                    if(n.getText().toString().equalsIgnoreCase(list.get(i).City)) {
                        filterlist.add(list.get(i));
                    }
                }
            }
            bind();
            LatLng l1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            LatLng l2 = new LatLng(Double.parseDouble(filterlist.get(0).lat), Double.parseDouble(filterlist.get(0).lng));

            mMap.addMarker(new MarkerOptions().position(l1).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));
            mMap.addMarker(new MarkerOptions().position(l2).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));

            new FetchURL(Map.this).execute(getUrl(l1, l2, "driving"), "driving");
        }

    }
    @Click
    public void soho(){
        n.setText("SOHO");
        normalView();
        filterlist.clear();
        if(n.getText().toString().equalsIgnoreCase("SOHO")){
            for(int i=0;i<list.size();i++) {
                if (mLastLocation != null) {
                    if(n.getText().toString().equalsIgnoreCase(list.get(i).City)) {
                        filterlist.add(list.get(i));
                    }
                }
            }
            bind();
            LatLng l1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            LatLng l2 = new LatLng(Double.parseDouble(filterlist.get(0).lat), Double.parseDouble(filterlist.get(0).lng));

            mMap.addMarker(new MarkerOptions().position(l1).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));
            mMap.addMarker(new MarkerOptions().position(l2).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));

            new FetchURL(Map.this).execute(getUrl(l1, l2, "driving"), "driving");
        }
    }
    @Click
    public void brooklyn(){
        n.setText("Brooklyn");
        normalView();
        filterlist.clear();
        if(n.getText().toString().equalsIgnoreCase("Brooklyn")){
            for(int i=0;i<list.size();i++) {
                if (mLastLocation != null) {
                    if(n.getText().toString().equalsIgnoreCase(list.get(i).City)) {
                        filterlist.add(list.get(i));
                    }
                }
            }
            bind();
            LatLng l1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            LatLng l2 = new LatLng(Double.parseDouble(filterlist.get(0).lat), Double.parseDouble(filterlist.get(0).lng));

            mMap.addMarker(new MarkerOptions().position(l1).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));
            mMap.addMarker(new MarkerOptions().position(l2).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));

            new FetchURL(Map.this).execute(getUrl(l1, l2, "driving"), "driving");
        }
    }



    // slide the view from below itself to the current position
    public void slideUp(View view){
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }
    public void slideDown(View view){
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    private void bindData(){


        list.add(new HotelModel("171 banker st., brooklyn, ny 11222","0.2 miles away",R.drawable.h1,"40.72589","-73.95609","Brooklyn"));
        list.add(new HotelModel("80 Wythe Avenue  Brooklyn  New York","0.3 miles away",R.drawable.h2,"40.72198","-73.95793","Brooklyn"));
        list.add(new HotelModel("123, HAVEMEYER STREET, BROOKLYN, NY, 11211","0.5 miles away",R.drawable.h3,"40.71616","-73.95265","Brooklyn"));
        list.add(new HotelModel("148 Meserole Street, Brooklyn, NY 11206","0.5 miles away",R.drawable.h4,"40.70784","-73.94376","Brooklyn"));
        list.add(new HotelModel("1022, Broadway, Brooklyn, NY","0.5 miles away",R.drawable.h1,"40.69537","-73.93292","Brooklyn"));

        list.add(new HotelModel("95, COMMERCIAL ST, GREENPOINT, BROOKLYN, NY, 11222","0.5 miles away",R.drawable.h1,"40.737712","-73.956129","Greenpoint"));
        list.add(new HotelModel("1103, Manhattan Ave, Brooklyn, NY, 11222","0.5 miles away",R.drawable.h2,"40.736228","-73.955568","Greenpoint"));
        list.add(new HotelModel("999, Manhattan Ave, Brooklyn, NY, 11222","0.5 miles away",R.drawable.h3,"40.73317","-73.955046","Greenpoint"));

        list.add(new HotelModel("731, Lexington Avenue Located inside, Beacon Ct, New York, NY, 10022","0.5 miles away",R.drawable.h1,"40.761732","-73.968378","Midtown"));
        list.add(new HotelModel("65, E 55th St, New York, NY, 10022","0.5 miles away",R.drawable.h2,"40.760887","-73.972129","Midtown"));
        list.add(new HotelModel("204, E 58th St, New York, NY, 10022","0.5 miles away",R.drawable.h3,"40.760584","-73.966621","Midtown"));

        list.add(new HotelModel("326, Spring St, New York, NY, 10013","0.5 miles away",R.drawable.h1,"40.725845","-74.009494","SOHO"));
        list.add(new HotelModel("234, Spring St, New York, NY, 10013","0.5 miles away",R.drawable.h2,"40.725466","-74.004975","SOHO"));
        list.add(new HotelModel("18, King St, New York, NY, 10014","0.5 miles away",R.drawable.h3,"40.727508","-74.003447","SOHO"));

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation=location;
        appPreferences.set("lat",location.getLatitude()+"");
        appPreferences.set("lng",location.getLongitude()+"");
//        CameraPosition cameraPosition = new CameraPosition.Builder()
//                .target(new LatLng(location.getLatitude(),location.getLongitude()))
//                .zoom(14F)
//                .build();
//        mMap.setMaxZoomPreference(100f);
//        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//        mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(),location.getLongitude())).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).flat(true));

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new com.google.android.gms.location.LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    mLastLocation=location;
                    appPreferences.set("lat",location.getLatitude()+"");
                    appPreferences.set("lng",location.getLongitude()+"");
//                    CameraPosition cameraPosition = new CameraPosition.Builder()
//                            .target(new LatLng(location.getLatitude(),location.getLongitude()))
//                            .zoom(14F)
//                            .build();
//                    mMap.setMaxZoomPreference(100f);
//                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                    mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(),location.getLongitude())).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).flat(true));
                }
            });
        }
        if(mLastLocation!=null) {
            LatLng l1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            LatLng l2 = new LatLng(Double.parseDouble(list.get(0).lat), Double.parseDouble(list.get(0).lng));

            mMap.addMarker(new MarkerOptions().position(l1).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));
            mMap.addMarker(new MarkerOptions().position(l2).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_ic)).flat(true));

            new FetchURL(Map.this).execute(getUrl(l1, l2, "driving"), "driving");

//                    CameraPosition cameraPosition = new CameraPosition.Builder()
//                            .target(new LatLng(l1.latitude, l1.longitude))
//                            .zoom(12.5F)
//                            .build();
//                    mMap.setMaxZoomPreference(100f);
//                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onTaskDone(Object... values) {
        if (currentPolyline != null)
            currentPolyline.remove();
        currentPolyline = mMap.addPolyline((PolylineOptions) values[0]);
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }



    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("PorterApp")
                .setMessage("Are you sure you want to close this activity?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent();
                        intent.setAction("doExit");
                        intent.putExtra("data","Notice me senpai!");
                        sendBroadcast(intent);


                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }
}
