package app.com.porterapp;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;

@EActivity(R.layout.dialog)
public class ActDialog extends AppCompatActivity {

    @AfterViews
    public void init(){

    }
    @Click
    public void close(){
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
    @Click
    public void skip(){
        startActivity(new Intent(this,HotelDetail_.class));
    }
    @Click
    public void explore(){
        finish();
    }
}
