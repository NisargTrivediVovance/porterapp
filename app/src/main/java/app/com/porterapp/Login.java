package app.com.porterapp;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.Polyline;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_login)
public class Login extends AppCompatActivity implements LocationListener,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    private Polyline currentPolyline;
    public Location mLastLocation;
    AppPreferences appPreferences;
    @ViewById
    EditText edtusername;
    @ViewById
    EditText edtPassword;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";



    @ViewById
    TextView singin;

    @AfterViews
    public void init(){

        appPreferences=new AppPreferences(this);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) Login.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(Login.this);
                alertBuilder.setCancelable(true);
                alertBuilder.setTitle("Permission necessary");
                alertBuilder.setMessage("Write calendar permission is necessary to write event!!!");
                alertBuilder.setPositiveButton(android.R.string.yes, (dialog, which) -> ActivityCompat.requestPermissions((Activity)Login.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1000));
                AlertDialog alert = alertBuilder.create();
                alert.show();

            } else {
                ActivityCompat.requestPermissions((Activity)Login.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1000);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        buildGoogleApiClient();
                        //mMap.setMyLocationEnabled(true);
                    }
                } else {
                    buildGoogleApiClient();
                    // mMap.setMyLocationEnabled(true);
                }
            }
        } else {
//            permission();
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    buildGoogleApiClient();
                    //mMap.setMyLocationEnabled(true);
                }
            } else {
                buildGoogleApiClient();
                // mMap.setMyLocationEnabled(true);
            }
        }
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }
    @Click
    public void btn(){
        if(TextUtils.isEmpty(edtusername.getText().toString())){
            Toast.makeText(Login.this,"Please enter username",Toast.LENGTH_LONG).show();
        }else if(!edtusername.getText().toString().trim().matches(emailPattern)){
            Toast.makeText(Login.this,"Please enter valid email",Toast.LENGTH_LONG).show();
        }else if(TextUtils.isEmpty(edtPassword.getText().toString())){
            Toast.makeText(Login.this,"Please enter password",Toast.LENGTH_LONG).show();
        }else finish();startActivity(new Intent(this,Skip_.class));
    }


    @Click
    public void singin(){
//        finish();
        startActivity(new Intent(Login.this,Signup_.class));
    }

    @Click
    public void quickSign(){
        finish();
    }
    @Click
    public void gp(){
        finish();
        startActivity(new Intent(this,Skip_.class));
    }
    @Click
    public void tw(){
        finish();
        startActivity(new Intent(this,Skip_.class));
    }
    @Click
    public void fb(){
        finish();
        startActivity(new Intent(this,Skip_.class));
    }

    @Override
    public void onLocationChanged(Location location) {

        appPreferences.set("lat",location.getLatitude()+"");
        appPreferences.set("lng",location.getLongitude()+"");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new com.google.android.gms.location.LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    appPreferences.set("lat",location.getLatitude()+"");
                    appPreferences.set("lng",location.getLongitude()+"");
//                    CameraPosition cameraPosition = new CameraPosition.Builder()
//                            .target(new LatLng(location.getLatitude(),location.getLongitude()))
//                            .zoom(14F)
//                            .build();
//                    mMap.setMaxZoomPreference(100f);
//                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                    mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(),location.getLongitude())).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).flat(true));
                }
            });
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
