package app.com.porterapp;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import androidx.fragment.app.Fragment;
import app.com.porterapp.utility.Utility;


@EFragment(R.layout.mexplorer)
//@EFragment(value = R.layout.mexplorer, forceLayoutInjection = true)
public class ExplorerFragment extends Fragment {


    @ViewById
    TextView tvHome;
    @ViewById
    TextView tvExplore;
    @ViewById
    TextView tvMap;
    @ViewById
    TextView tvLikes;
    @ViewById
    TextView tvProfile;

    @ViewById
    TextView topbrookline;

    @ViewById
    ImageView arrowExplore;

    @ViewById
    ImageView imgExit;

    @ViewById
    TextView nyc;

    @ViewById
    TextView txtdining;

    @ViewById
    TextView txtshoping;

    @ViewById
    TextView topic;

    @ViewById
    LinearLayout linear1;

    @ViewById
    LinearLayout linear2;

    @ViewById
    ImageView imgstatueofliberty;

    @ViewById
    LinearLayout linear1_1;


    @ViewById
    FrameLayout rfrm;


    @ViewById
    FrameLayout FrmClose;


    @ViewById
    ImageView imgsoho;


    @ViewById
    TextView  changeLocationleft;

    @ViewById
    TextView  topExplorer;

    @ViewById
    LinearLayout headrlinearbg;


    @ViewById
    TextView  changeLocation;


    Context mContext;

    boolean isDown = true;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;

    }


    @AfterViews
    void calledAfterviewInjection(){

        setSelected(tvExplore, tvHome, tvMap, tvLikes, tvProfile);
        TopicSelected();
    }

    @Click
    public  void txtshoping(){

        txtshoping.setBackground(getResources().getDrawable(R.drawable.boarder));
        txtshoping.setTextColor(getResources().getColor(R.color.textColor));
        txtdining.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtdining.setTextColor(getResources().getColor(R.color.black));
        topic.setBackground(getResources().getDrawable(android.R.color.transparent));
        topic.setTextColor(getResources().getColor(R.color.black));

//        finish();
//        startActivity(new Intent(mContext, Shoppinglist_.class));


        ShoppinglistFragment fragment = ShoppinglistFragment_.builder().build();
        Utility.SetFragment(fragment, mContext, "Shopnglist");

    }

    @Click
    public void topic(){
        TopicSelected();
    }


    public void TopicSelected(){

        topic.setBackground(getResources().getDrawable(R.drawable.boarder));
        topic.setTextColor(getResources().getColor(R.color.textColor));
        txtdining.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtdining.setTextColor(getResources().getColor(R.color.black));
        txtshoping.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtshoping.setTextColor(getResources().getColor(R.color.black));

    }

    @Click
    public void txtdining(){
        //Toast.makeText(getApplicationContext(),"Click",Toast.LENGTH_LONG).show();
        txtdining.setBackground(getResources().getDrawable(R.drawable.boarder));
        txtdining.setTextColor(getResources().getColor(R.color.textColor));

        topic.setBackground(getResources().getDrawable(android.R.color.transparent));
        topic.setTextColor(getResources().getColor(R.color.black));

        txtshoping.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtshoping.setTextColor(getResources().getColor(R.color.black));

//        finish();
//        startActivity(new Intent(mContext, Dininglist_.class));



        DinniglistFragment fragment = DinniglistFragment_.builder().build();
        Utility.SetFragment(fragment, mContext, "Explorer");

    }
    @Click
    public  void linear2(){
        //finish();
//        startActivity(new Intent(mContext, Imagesstatueofliberty_.class));
    }


    @Click
    public  void imgsoho(){
        //finish();
        startActivity(new Intent(mContext, Imagesstatueofliberty_.class));
    }


    @Click
    public  void imgstatueofliberty(){
        //finish();
        startActivity(new Intent(mContext, Imagesstatueofliberty_.class));
    }

    @Click
    public void tvHome() {
        startActivity(new Intent(mContext, Home_.class));
    }

    @Click
    public void tvExplore() {
//        startActivity(new Intent(mContext, Explore_.class));
    }

    @Click
    public void tvMap() {
        startActivity(new Intent(mContext, Map_.class));
    }

    @Click
    public void tvLikes() {
        startActivity(new Intent(mContext, Likes_.class));
    }

    @Click
    public void tvProfile() {
        startActivity(new Intent(mContext, Profile_.class));
    }

    /*@Override
    public void onBackPressed() {
        finish();
    }*/


    public void setSelected(TextView tv1, TextView tv2, TextView tv3, TextView tv4, TextView tv5) {
        tv1.setSelected(true);
        tv1.setTextColor(getResources().getColor(android.R.color.white));
        tv2.setSelected(false);
        tv2.setTextColor(getResources().getColor(R.color.bottom_color));
        tv3.setSelected(false);
        tv3.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv4.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv5.setTextColor(getResources().getColor(R.color.bottom_color));
        topbrookline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManageBrooklyn();
            }
        });
        arrowExplore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManageBrooklyn();
            }
        });


        imgExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManageBrooklyn();
            }
        });

        FrmClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManageBrooklyn();
            }
        });


    }


    public void ManageBrooklyn() {

        if (isDown) {
            isDown = false;
            arrowExplore.setRotation(270f);
            imgExit.setVisibility(View.VISIBLE);
            FrmClose.setVisibility(View.VISIBLE);

            linear1.setVisibility(View.GONE);
            linear1_1.setVisibility(View.VISIBLE);
            linear2.setVisibility(View.VISIBLE);
//            rfrm.setVisibility(View.GONE);

            headrlinearbg.setBackgroundColor(getResources().getColor(R.color.bgtransperant));
            nyc.setVisibility(View.GONE);

            topExplorer.setVisibility(View.GONE);
            changeLocation.setVisibility(View.GONE);
            changeLocationleft.setVisibility(View.VISIBLE);

        } else {

            isDown = true;
            arrowExplore.setRotation(180f);
            imgExit.setVisibility(View.GONE);
            FrmClose.setVisibility(View.GONE);
            nyc.setVisibility(View.VISIBLE);
            linear2.setVisibility(View.GONE);
            linear1_1.setVisibility(View.GONE);
            linear1.setVisibility(View.VISIBLE);
            rfrm.setVisibility(View.VISIBLE);

            topExplorer.setVisibility(View.VISIBLE);
            changeLocation.setVisibility(View.VISIBLE);
            changeLocationleft.setVisibility(View.GONE);

            headrlinearbg.setBackgroundColor(getResources().getColor(android.R.color.transparent));

        }

    }
}
