package app.com.porterapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.profile)
public class Profile extends AppCompatActivity {

    @ViewById
    TextView tvHome;
    @ViewById
    TextView tvExplore;
    @ViewById
    TextView tvMap;
    @ViewById
    TextView tvLikes;
    @ViewById
    TextView tvProfile;
    @AfterViews
    public void init()
    {
        setSelected(tvProfile,tvExplore,tvHome,tvMap,tvLikes);
    }

    public void setSelected(TextView tv1,TextView tv2,TextView tv3,TextView tv4,TextView tv5){
        tv1.setSelected(true);
        tv1.setTextColor(getResources().getColor(R.color.white));
        tv2.setSelected(false);
        tv2.setTextColor(getResources().getColor(R.color.bottom_color));
        tv3.setSelected(false);
        tv3.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv4.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv5.setTextColor(getResources().getColor(R.color.bottom_color));
    }
    @Click
    public void tvHome(){
        finish();
        startActivity(new Intent(this,Home_.class));
    }
    @Click
    public void tvExplore(){
        finish();
        startActivity(new Intent(this,Explore_.class));
    }
    @Click
    public void tvMap(){
        finish();
        startActivity(new Intent(this, Map_.class));
    }
    @Click
    public void tvLikes(){
        finish();
        startActivity(new Intent(this,Likes_.class));
    }
    @Click
    public void tvProfile(){
        finish();
        startActivity(new Intent(this,Profile_.class));
    }

    /*@Override
    public void onBackPressed() {
        finish();
    }*/


    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("PorterApp")
                .setMessage("Are you sure you want to close this activity?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent();
                        intent.setAction("doExit");
                        intent.putExtra("data","Notice me senpai!");
                        sendBroadcast(intent);

                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

}
