package app.com.porterapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {


    BroadcastReceiverFinish  ActFinsh;



    @AfterViews
    public void init(){



        this.ActFinsh = new BroadcastReceiverFinish();
        //return START_NOT_STICKY;
        this.registerReceiver(this.ActFinsh,  new IntentFilter("doExit"));

    }

    @Click
    public void btn(){
//        finish();
        startActivity(new Intent(MainActivity.this,Login_.class));
    }
    @Click
    public void singin(){
//        finish();
        startActivity(new Intent(MainActivity.this,Signup_.class));
    }


    public class BroadcastReceiverFinish extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            //contentTxt.setText(String.valueOf(level) + "%");
            finish();
        }
    };



    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("PorterApp")
                .setMessage("Are you sure you want to close this activity?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                       /* Intent intent = new Intent();
                        intent.setAction("doExit");
                        intent.putExtra("data","Notice me senpai!");
                        sendBroadcast(intent);
*/
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }
}
