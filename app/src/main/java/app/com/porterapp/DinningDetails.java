package app.com.porterapp;

import android.content.Intent;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import app.com.porterapp.adapter.DinningAdapter;
import app.com.porterapp.minterface.ItemRecycleClick;
import app.com.porterapp.pojo.DiningData;

@EActivity(R.layout.dinninglist)
public class DinningDetails extends AppCompatActivity {

    DinningAdapter mAdapter;

    @ViewById
    TextView tvHome;
    @ViewById
    TextView tvExplore;
    @ViewById
    TextView tvMap;
    @ViewById
    TextView tvLikes;
    @ViewById
    TextView tvProfile;
    @ViewById
    RecyclerView mRecyclerView;
    List<DiningData> mDl;

    @ViewById
    TextView txtdining;

    @ViewById
    TextView txtshoping;

    @ViewById
    TextView topic;


//    public  static int ItemPosition = 0;

    @AfterViews
    public void init()
    {
        setSelected(tvLikes,tvExplore,tvHome,tvMap,tvProfile);


//        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);


        prepareData();

        try {
            mAdapter = new DinningAdapter(DinningDetails.this,mDl, new ItemRecycleClick() {
                @Override
                public void RecycleClick(int pos) {

//                    ItemPosition = pos;
                    startActivity(new Intent(DinningDetails.this, ImagesDetails_.class));


                }
            });

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        DiningSelected();
    }



    private void prepareData(){

        mDl = new ArrayList<DiningData>();

        DiningData obj = new DiningData();
        obj.setName(R.drawable.dinning_details_1);
        mDl.add(obj);


        DiningData obj2 = new DiningData();
        obj2.setName(R.drawable.dinning_details_2);
        mDl.add(obj2);

        DiningData obj3 = new DiningData();
        obj3.setName(R.drawable.dinning_details_3);
        mDl.add(obj3);


        DiningData obj4 = new DiningData();
        obj4.setName(R.drawable.dinning_details_4);
        mDl.add(obj4);


        DiningData obj5 = new DiningData();
        obj5.setName(R.drawable.shopping_detail_4);
        mDl.add(obj5);


        DiningData obj6 = new DiningData();
        obj6.setName(R.drawable.shopping_detail_4);
        mDl.add(obj6);


        DiningData obj7 = new DiningData();
        obj7.setName(R.drawable.shopping_detail_4);
        mDl.add(obj7);
    }

    public void setSelected(TextView tv1,TextView tv2,TextView tv3,TextView tv4,TextView tv5){

        tv1.setSelected(false);
        tv1.setTextColor(getResources().getColor(R.color.bottom_color));
        tv2.setSelected(true);
        tv2.setTextColor(getResources().getColor(R.color.white));

        tv3.setSelected(false);
        tv3.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv4.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv5.setTextColor(getResources().getColor(R.color.bottom_color));
    }

    @Click
    public void tvHome(){
        startActivity(new Intent(this, Home_.class));
    }
    @Click
    public void tvExplore(){
        startActivity(new Intent(this, Explore_.class));
    }
    @Click
    public void tvMap(){
        startActivity(new Intent(this, Map_.class));
    }
    @Click
    public void tvLikes(){
        startActivity(new Intent(this, Likes_.class));
    }
    @Click
    public void tvProfile(){
        startActivity(new Intent(this, Profile_.class));
    }
    @Override
    public void onBackPressed() {
        finish();
    }






    @Click
    public  void txtshoping(){

        txtshoping.setBackground(getResources().getDrawable(R.drawable.boarder));
        txtshoping.setTextColor(getResources().getColor(R.color.textColor));

        txtdining.setBackground(getResources().getDrawable(R.color.white));
        txtdining.setTextColor(getResources().getColor(R.color.black));

        topic.setBackground(getResources().getDrawable(R.color.white));
        topic.setTextColor(getResources().getColor(R.color.black));

        startActivity(new Intent(this, Shoppinglist_.class));
    }


    @Click
    public void topic(){
        topic.setBackground(getResources().getDrawable(R.drawable.boarder));
        topic.setTextColor(getResources().getColor(R.color.textColor));
        txtdining.setBackground(getResources().getDrawable(R.color.white));
        txtdining.setTextColor(getResources().getColor(R.color.black));
        txtshoping.setBackground(getResources().getDrawable(R.color.white));
        txtshoping.setTextColor(getResources().getColor(R.color.black));

        startActivity(new Intent(this, Explore_.class));
    }

    @Click
    public void txtdining(){
        //Toast.makeText(getApplicationContext(),"Click",Toast.LENGTH_LONG).show();
        DiningSelected();
    }



    private void DiningSelected(){

        txtdining.setBackground(getResources().getDrawable(R.drawable.boarder));
        txtdining.setTextColor(getResources().getColor(R.color.textColor));

        topic.setBackground(getResources().getDrawable(R.color.white));
        topic.setTextColor(getResources().getColor(R.color.black));

        txtshoping.setBackground(getResources().getDrawable(R.color.white));
        txtshoping.setTextColor(getResources().getColor(R.color.black));

//        startActivity(new Intent(this, Dininglist_.class));

    }
}
