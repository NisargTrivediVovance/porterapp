package app.com.porterapp;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

public class HotelModel implements Serializable {

    public String HotelName="";
    public String KM="";
    public int HotelImage;
    public String lat="";
    public String lng="";
    public String City="";

    public HotelModel(String name,String km,int hotelImage,String lat,String lng,String city){
        this.HotelName=name;
        this.KM=km;
        this.HotelImage=hotelImage;
        this.lat=lat;
        this.lng=lng;
        this.City=city;
    }
}
