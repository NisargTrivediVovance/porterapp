package app.com.porterapp;

import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_skip)
public class Skip extends AppCompatActivity {

    @ViewById
    TextView tv;

    @ViewById
    TextView tv1;
    @ViewById
    TextView tv2;
    @ViewById
    TextView tv3;


    @AfterViews
    public void init(){

    }
    @Click
    public void tv(){
        if(tv1.getVisibility()==View.VISIBLE){
            tv1.setVisibility(View.GONE);
            tv2.setVisibility(View.GONE);
            tv3.setVisibility(View.GONE);
            tv.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.left,0);
            tv.setText("Marriott Times Square");
        }else {
            tv1.setVisibility(View.VISIBLE);
            tv2.setVisibility(View.VISIBLE);
            tv3.setVisibility(View.VISIBLE);
            tv.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.down,0);
            tv.setText("Search...");
        }
    }

    @Click
    public void tv1(){
        if(tv1.getVisibility()==View.VISIBLE){
            tv1.setVisibility(View.GONE);
            tv2.setVisibility(View.GONE);
            tv3.setVisibility(View.GONE);
            tv.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.left,0);
            tv.setText("Marriott Times Square");
        }else {
            tv1.setVisibility(View.VISIBLE);
            tv2.setVisibility(View.VISIBLE);
            tv3.setVisibility(View.VISIBLE);
            tv.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.down,0);
            tv.setText("Search...");
        }
    }

    @Click
    public void tv2(){
        if(tv1.getVisibility()==View.VISIBLE){
            tv1.setVisibility(View.GONE);
            tv2.setVisibility(View.GONE);
            tv3.setVisibility(View.GONE);
            tv.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.left,0);
            tv.setText("Hilton Times Square");
        }else {
            tv1.setVisibility(View.VISIBLE);
            tv2.setVisibility(View.VISIBLE);
            tv3.setVisibility(View.VISIBLE);
            tv.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.down,0);
            tv.setText("Search...");
        }
    }
    @Click
    public void tv3(){
        if(tv1.getVisibility()==View.VISIBLE){
            tv1.setVisibility(View.GONE);
            tv2.setVisibility(View.GONE);
            tv3.setVisibility(View.GONE);
            tv.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.left,0);
            tv.setText("Holiday Inn Midtown");
        }else {
            tv1.setVisibility(View.VISIBLE);
            tv2.setVisibility(View.VISIBLE);
            tv3.setVisibility(View.VISIBLE);
            tv.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.down,0);
            tv.setText("Search...");
        }
    }

    @Click
    public void skip(){
        finish();
        startActivity(new Intent(this,Explore_.class));
    }
    @Click
    public void add(){
        finish();
        startActivity(new Intent(this,Explore_.class).putExtra("fromadd","yes"));
    }

}
