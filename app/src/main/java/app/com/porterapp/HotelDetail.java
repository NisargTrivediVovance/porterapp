package app.com.porterapp;

import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.hotel_detail)
public class HotelDetail extends AppCompatActivity {



    @ViewById
    ImageView close;
    @AfterViews
    public void init(){

    }


    @Override
    public void onBackPressed() {
        finish();
    }


    @Click
    public void close(){
        finish();
    }
}
