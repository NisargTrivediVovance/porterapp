package app.com.porterapp;

import android.content.Intent;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_singup)
public class Signup extends AppCompatActivity {

    @ViewById
    EditText edtName;

    @ViewById
    EditText edtPhone;

    @ViewById
    EditText edtUsername;

    @ViewById
    EditText edtEmail;

    @ViewById
    EditText edtPassword;

    @ViewById
    TextView quickSign;


    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    @AfterViews
    public void init(){

    }
    @Click
    public void quickSign(){
        finish();
    }
    @Click
    public void btn(){
        if(TextUtils.isEmpty(edtName.getText().toString())){
            Toast.makeText(Signup.this,"Please enter name",Toast.LENGTH_LONG).show();
        }else if(TextUtils.isEmpty(edtPhone.getText().toString())){
            Toast.makeText(Signup.this,"Please enter phone",Toast.LENGTH_LONG).show();
        }else if(edtPhone.getText().length()<10){
            Toast.makeText(Signup.this,"Please enter proper phone number",Toast.LENGTH_LONG).show();
        }else if(TextUtils.isEmpty(edtUsername.getText().toString())){
            Toast.makeText(Signup.this,"Please enter username",Toast.LENGTH_LONG).show();
        }else if(TextUtils.isEmpty(edtEmail.getText().toString())){
            Toast.makeText(Signup.this,"Please enter email",Toast.LENGTH_LONG).show();
        }else if(!edtEmail.getText().toString().trim().matches(emailPattern)){
            Toast.makeText(Signup.this,"Please enter valid email",Toast.LENGTH_LONG).show();
        }else if(TextUtils.isEmpty(edtPassword.getText().toString())){
            Toast.makeText(Signup.this,"Please enter password",Toast.LENGTH_LONG).show();
        }else{ finish();startActivity(new Intent(this,Skip_.class));}
    }
    @Click
    public void gp(){
        finish();
        startActivity(new Intent(this,Skip_.class));
    }
    @Click
    public void tw(){
        finish();
        startActivity(new Intent(this,Skip_.class));
    }
    @Click
    public void fb(){
        finish();
        startActivity(new Intent(this,Skip_.class));
    }


}
