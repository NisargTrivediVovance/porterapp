package app.com.porterapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import androidx.appcompat.app.AppCompatActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;


import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import app.com.porterapp.adapter.DinningAdapter;
import app.com.porterapp.adapter.ShopingDetailsAdapte;
import app.com.porterapp.minterface.ItemRecycleClick;
import app.com.porterapp.pojo.DiningData;


@EActivity(R.layout.dinninglist)
public class Dininglist extends AppCompatActivity {

    DinningAdapter  mAdapter;

    @ViewById
    TextView tvHome;
    @ViewById
    TextView tvExplore;
    @ViewById
    TextView tvMap;
    @ViewById
    TextView tvLikes;
    @ViewById
    TextView tvProfile;
    @ViewById
    RecyclerView mRecyclerView;
    List<DiningData> mDl;

    @ViewById
    TextView txtdining;

    @ViewById
    TextView txtshoping;

    @ViewById
    TextView topic;


    BroadcastReceiverFinish ActFinsh;

   @AfterViews
    public void init()
    {


        this.ActFinsh = new BroadcastReceiverFinish();
        //return START_NOT_STICKY;
        this.registerReceiver(this.ActFinsh,  new IntentFilter("doExit"));
        setSelected(tvLikes,tvExplore,tvHome,tvMap,tvProfile);


//        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);

        Dining_List();
        DiningSelected();
    }



    private void prepareData(){

        mDl = new ArrayList<DiningData>();

        DiningData obj = new DiningData();
        obj.setName(R.drawable.dinning1);
//        obj.setName(R.drawable.dinning_image_1);

        mDl.add(obj);


        DiningData obj2 = new DiningData();
        obj2.setName(R.drawable.dinning2);
        mDl.add(obj2);

        DiningData obj3 = new DiningData();
        obj3.setName(R.drawable.dinning3);
        mDl.add(obj3);


        DiningData obj4 = new DiningData();
        obj4.setName(R.drawable.dinning4);
        mDl.add(obj4);

        DiningData obj5 = new DiningData();
        obj5.setName(R.drawable.dinning5);
        mDl.add(obj5);
    }

    public void setSelected(TextView tv1,TextView tv2,TextView tv3,TextView tv4,TextView tv5){
        tv1.setSelected(false);
        tv1.setTextColor(getResources().getColor(R.color.bottom_color));
        tv2.setSelected(true);
        tv2.setTextColor(getResources().getColor(R.color.white));

        tv3.setSelected(false);
        tv3.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv4.setTextColor(getResources().getColor(R.color.bottom_color));
        tv4.setSelected(false);
        tv5.setTextColor(getResources().getColor(R.color.bottom_color));
    }

    @Click
    public void tvHome(){
        finish();
        startActivity(new Intent(this, Home_.class));
    }
    @Click
    public void tvExplore(){
        finish();
        startActivity(new Intent(this, Explore_.class));
    }
    @Click
    public void tvMap(){
        finish();
        startActivity(new Intent(this, Map_.class));
    }
    @Click
    public void tvLikes(){
        finish();
        startActivity(new Intent(this, Likes_.class));
    }
    @Click
    public void tvProfile(){
        finish();
        startActivity(new Intent(this, Profile_.class));
    }
    @Override
    public void onBackPressed() {
        finish();
    }






    @Click
    public  void txtshoping(){

        txtshoping.setBackground(getResources().getDrawable(R.drawable.boarder));
        txtshoping.setTextColor(getResources().getColor(R.color.textColor));

        txtdining.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtdining.setTextColor(getResources().getColor(R.color.black));

        topic.setBackground(getResources().getDrawable(android.R.color.transparent));
        topic.setTextColor(getResources().getColor(R.color.black));


//        finish();
//        startActivity(new Intent(this, Shoppinglist_.class));

        ShppingSelected();

        Shoping_List();
    }


    @Click
    public void topic(){
        topic.setBackground(getResources().getDrawable(R.drawable.boarder));
        topic.setTextColor(getResources().getColor(R.color.textColor));
        txtdining.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtdining.setTextColor(getResources().getColor(R.color.black));
        txtshoping.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtshoping.setTextColor(getResources().getColor(R.color.black));
        finish();
        startActivity(new Intent(this, Explore_.class));
    }

    @Click
    public void txtdining(){
        //Toast.makeText(getApplicationContext(),"Click",Toast.LENGTH_LONG).show();
        DiningSelected();
        Dining_List();
    }



    private void DiningSelected(){

        txtdining.setBackground(getResources().getDrawable(R.drawable.boarder));
        txtdining.setTextColor(getResources().getColor(R.color.textColor));

        topic.setBackground(getResources().getDrawable(android.R.color.transparent));
        topic.setTextColor(getResources().getColor(R.color.black));

        txtshoping.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtshoping.setTextColor(getResources().getColor(R.color.black));

//        startActivity(new Intent(this, Dininglist_.class));

    }

    //-------------------------------------------------------1. DiningList Data-------------------------------

    public  void Dining_List(){

        prepareData();

        try {
            mAdapter = new DinningAdapter(Dininglist.this,mDl, new ItemRecycleClick() {
                @Override
                public void RecycleClick(int pos) {

                   // startActivity(new Intent(Dininglist.this, DinningDetails_.class));

                    if(pos<1) {
                        Details_Scren_process_here();
                    }



                }
            });

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


//--------------------------------------------------------------Details Screen Data-------------------------------------
    public  static int ItemPosition = 0;

    public void Details_Scren_process_here(){

        PrepareData_Details();

        try {
            mAdapter = new DinningAdapter(Dininglist.this,mDl, new ItemRecycleClick() {
                @Override
                public void RecycleClick(int pos) {

                    ItemPosition = pos;
                    startActivity(new Intent(Dininglist.this, ImagesDetails_.class));


                }
            });

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        DiningSelected();

    }


    public  void PrepareData_Details(){

        mDl = new ArrayList<DiningData>();

        DiningData obj = new DiningData();
        obj.setName(R.drawable.dinning_details_1);
        mDl.add(obj);


        DiningData obj2 = new DiningData();
        obj2.setName(R.drawable.dinning_details_2);
        mDl.add(obj2);

        DiningData obj3 = new DiningData();
        obj3.setName(R.drawable.dinning_details_3);
        mDl.add(obj3);


        DiningData obj4 = new DiningData();
        obj4.setName(R.drawable.dinning_details_4);
        mDl.add(obj4);


        DiningData obj5 = new DiningData();
        obj5.setName(R.drawable.shopping_detail_4);
        mDl.add(obj5);


        DiningData obj6 = new DiningData();
        obj6.setName(R.drawable.shopping_detail_4);
        mDl.add(obj6);


        DiningData obj7 = new DiningData();
        obj7.setName(R.drawable.shopping_detail_4);
        mDl.add(obj7);
    }

    //-------------------------------------------------------2. ShopingList Data-------------------------------

    private void ShppingSelected(){

        txtshoping.setBackground(getResources().getDrawable(R.drawable.boarder));
        txtshoping.setTextColor(getResources().getColor(R.color.textColor));

        txtdining.setBackground(getResources().getDrawable(android.R.color.transparent));
        txtdining.setTextColor(getResources().getColor(R.color.black));

        topic.setBackground(getResources().getDrawable(android.R.color.transparent));
        topic.setTextColor(getResources().getColor(R.color.black));
    }


    public  void Shoping_List(){

        prepareData_shopinglist();

        try {
            mAdapter = new DinningAdapter(Dininglist.this,mDl, new ItemRecycleClick() {
                @Override
                public void RecycleClick(int pos) {
                    // startActivity(new Intent(Shoppinglist.this, ShoppinglistDetails_.class));

                    if(pos==0) {
                        Details_Scren_process_shoping_here();
                    }

                }
            });

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    private void prepareData_shopinglist(){

        mDl = new ArrayList<DiningData>();

        DiningData obj = new DiningData();
        obj.setName(R.drawable.shopping_1);
        mDl.add(obj);


        DiningData obj2 = new DiningData();
        obj2.setName(R.drawable.shopping_2);
        mDl.add(obj2);

        DiningData obj3 = new DiningData();
        obj3.setName(R.drawable.shopping_3);
        mDl.add(obj3);


        DiningData obj4 = new DiningData();
        obj4.setName(R.drawable.shopping_4);
        mDl.add(obj4);

        DiningData obj5 = new DiningData();
        obj5.setName(R.drawable.shopping_5);
        mDl.add(obj5);

        DiningData obj6 = new DiningData();
        obj6.setName(R.drawable.shopping_6);
        mDl.add(obj6);

        DiningData obj7 = new DiningData();
        obj7.setName(R.drawable.shopping_7);
        mDl.add(obj7);

    }


    //-------------------------------------------------------------- Details Screen Data-------------------------------------

    public void Details_Scren_process_shoping_here(){

        ShopingDetailsAdapte mAdapter;

        PrepareData_Shoping_Details();

        try {
            mAdapter = new ShopingDetailsAdapte(Dininglist.this,mDl, new ItemRecycleClick() {
                @Override
                public void RecycleClick(int pos) {

                    if(pos<=1) {
                        ItemPosition = pos;
                        startActivity(new Intent(Dininglist.this, ImagesDetailsShopping_.class));
                    }

                }
            });

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public  void PrepareData_Shoping_Details(){

        mDl = new ArrayList<DiningData>();

        DiningData obj = new DiningData();
        obj.setName(R.drawable.shopping_detail_1);
        mDl.add(obj);

        DiningData obj2 = new DiningData();
        obj2.setName(R.drawable.shopping_detail_2);
        mDl.add(obj2);

        DiningData obj3 = new DiningData();
        obj3.setName(R.drawable.shopping_detail_3);
        mDl.add(obj3);

        DiningData obj4 = new DiningData();
        obj4.setName(R.drawable.shopping_detail_4);
        mDl.add(obj4);

        DiningData obj5 = new DiningData();
        obj5.setName(R.drawable.shopping_detail_4);
        mDl.add(obj5);

        DiningData obj6 = new DiningData();
        obj6.setName(R.drawable.shopping_detail_4);
        mDl.add(obj6);

        DiningData obj7 = new DiningData();
        obj7.setName(R.drawable.shopping_detail_4);
        mDl.add(obj7);

    }



    public class BroadcastReceiverFinish extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            //contentTxt.setText(String.valueOf(level) + "%");
            finish();
        }
    };

}